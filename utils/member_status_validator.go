/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package utils

import (
	"chainmaker.org/chainmaker/net-common/common"
	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
)

func MemberStatusValidateWithCertMode(
	memberStatusValidator *common.MemberStatusValidator,
	certBytes []byte) (chainIds []string, passed bool, err error) {
	m := &pbac.Member{
		OrgId:      "",
		MemberType: pbac.MemberType_CERT,
		MemberInfo: certBytes,
	}
	return memberStatusValidator.ValidateMemberStatus([]*pbac.Member{m})
}

func ChainMemberStatusValidateWithCertMode(
	chainId string,
	memberStatusValidator *common.MemberStatusValidator,
	certBytes []byte) (passed bool, err error) {
	m := &pbac.Member{
		OrgId:      "",
		MemberType: pbac.MemberType_CERT,
		MemberInfo: certBytes,
	}
	return memberStatusValidator.ValidateMemberStatusWithChain([]*pbac.Member{m}, chainId)
}

func MemberStatusValidateWithPubKeyMode(
	memberStatusValidator *common.MemberStatusValidator,
	pubKeyBytes []byte) (chainIds []string, passed bool, err error) {
	m := &pbac.Member{
		OrgId:      "",
		MemberType: pbac.MemberType_PUBLIC_KEY,
		MemberInfo: pubKeyBytes,
	}
	return memberStatusValidator.ValidateMemberStatus([]*pbac.Member{m})
}

func ChainMemberStatusValidateWithPubKeyMode(
	chainId string,
	memberStatusValidator *common.MemberStatusValidator,
	pubKeyBytes []byte) (passed bool, err error) {
	m := &pbac.Member{
		OrgId:      "",
		MemberType: pbac.MemberType_PUBLIC_KEY,
		MemberInfo: pubKeyBytes,
	}
	return memberStatusValidator.ValidateMemberStatusWithChain([]*pbac.Member{m}, chainId)
}
