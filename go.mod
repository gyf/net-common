module chainmaker.org/chainmaker/net-common

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/pb-go/v2 v2.2.0
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/stretchr/testify v1.7.0
	github.com/xiaotianfork/q-tls-common v0.1.4
	github.com/xiaotianfork/quic-go v0.26.0
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf
)

replace github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v1.0.0
